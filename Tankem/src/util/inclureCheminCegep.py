## -*- coding: utf-8 -*-
#On importe manuellement les chemins qui incluent les modules de Panda3D
#On a pas le choix car plusieurs versions de Python sont installés
#et on ne  plus pas utiliser la manière standard pour ajouter des librarires
#(utliser fichier .pth ou encore ajouter le chekmin dans la variable d'environnement
# PATH)
import sys

sys.path.insert(0, "C:\\Logiciels Portables\\Panda3D-1.9.1-x64\\bin")
sys.path.insert(0, "C:\\Logiciels Portables\\Panda3D-1.9.1-x64")
sys.path.insert(0, "C:\\Logiciels Portables\\Panda3D-1.9.1-x64\\python\\Lib\\site-packages")